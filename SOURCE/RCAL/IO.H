/*
 * rcal - a "rolling" calculator
 * Copyright 2018 (C) Mateusz Viste
 *
 * http://rcal.sourceforge.net
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef io_h
#define io_h

int io_getkey(void);
void io_getcursorpos(int *row, int *col);
void io_setcursorpos(int row, int col);
void io_setcursorxpos(int col);
void io_movcursorx(int delta);
void io_printdosstr(char *s);
void io_printdoschar(char c);
int io_getcountryparms(void *buf);
int io_fcreat(char *f, unsigned short *handle);
int io_fwrite(unsigned short handle, void *s, unsigned short len);
int io_fclose(unsigned short handle);

#endif
