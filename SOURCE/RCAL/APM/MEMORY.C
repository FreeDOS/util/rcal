/******************************************************************************

	Arbitrary Precision Math Library General Public License
		    (Written October 5, 1988)

 Copyright (C) 1988 Lloyd Zusman, Master Byte Software, Los
 Gatos, California.  Everyone is permitted to copy and distribute
 verbatim copies of this license, but changing it is not allowed.
 You can also use this wording to make the terms for other programs.

 The wording of this license is based on that of the
 "GNU EMACS GENERAL PUBLIC LICENSE" by Richard Stallman,
 Copyright (C) 1985, 1987, 1988, version of February 11, 1988,
 but since some of the text has been changed, please be sure to
 READ THIS CAREFULLY!

  This general public license is intended to give everyone the right
to share the Arbitrary Precision Math Library (hereinafter referred to
as the "APM Library").  To make sure that you get the rights we want
you to have, I need to make restrictions that forbid anyone to deny
you these rights or to ask you to surrender the rights.

  Specifically, we want to make sure that you have the right to give
away copies of the APM Library, that you receive source code or else
can get it if you want it, that you can change the APM Library or use
pieces of it in new programs, and that you know you can do these
things.

  To make sure that everyone has such rights, we have to forbid you to
deprive anyone else of these rights.  For example, if you distribute
copies of the APM Library, you must give the recipients all the
rights that you have.  You must make sure that they, too, receive or
can get the source code.  And you must tell them their rights.

  Also, for our own protection, we must make certain that everyone
finds out that there is no warranty for the APM Library.  If the APM
Library is modified by someone else and passed on, we want its
recipients to know that what they have is not what we distributed, so
that any problems introduced by others will not reflect on our
reputation.

  Therefore we (Lloyd Zusman and Master Byte Software) make the
following terms which say what you must do to be allowed to
distribute or change the APM Library.

			COPYING POLICIES

1. You may copy and distribute verbatim copies of the APM Library
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy a valid copyright
notice "Copyright (C) 1988 Lloyd Zusman, Master Byte Software, Los
Gatos, California" (or with whatever year is appropriate); keep intact
the notices on all files that refer to this License Agreement and to
the absence of any warranty; and give any other recipients of the the
APM Library program a copy of this License Agreement along with the
program.  You may charge a distribution fee for the physical act of
transferring a copy.

  2. You may modify your copy or copies of the APM Library source code or
any portion of it, and copy and distribute such modifications under
the terms of Paragraph 1 above, provided that you also do the following:

    a) cause the modified files to carry prominent notices stating
    that you changed the files and the date of any change; and

    b) cause the whole of any work that you distribute or publish, that in
    whole or in part contains or is a derivative of the APM Library or any
    part thereof, to be licensed to all third parties on terms identical
    to those contained in this License Agreement (except that you may
    choose to grant more extensive warranty protection to some or all
    third parties, at your option).

    c) You may charge a distribution fee for the physical act of
    transferring a copy, and you may at your option offer warranty
    protection in exchange for a fee.

    d) You may not charge a license fee for the whole of any work that
    you distribute or publish, that in whole or in part contains or is
    a derivative of the APM library or any part thereof, without the
    express written permission of Lloyd Zusman and Master Byte Software;
    whether this permission is granted for free or in return for goods
    services, royalties, or other compensation will be determined
    solely by Lloyd Zusman and Master Byte Software.

Mere aggregation of another unrelated program with this program (or its
derivative) on a volume of a storage or distribution medium does not bring
the other program under the scope of these terms.

  3. You may copy and distribute the APM Library (or a portion or
derivative of it, under Paragraph 2) in object code or executable form
under all the terms of Paragraphs 1 and 2 above provided that you also
do one of the following:

    a) accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of
    Paragraphs 1 and 2 above; or,

    b) accompany it with a written offer, valid for at least three
    years, to give any third party free (except for a nominal
    shipping charge) a complete machine-readable copy of the
    corresponding source code, to be distributed under the terms of
    Paragraphs 1 and 2 above; or,

    c) accompany it with the information you received as to where the
    corresponding source code may be obtained.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form alone.)

For an executable file, complete source code means all the source code
for all modules it contains; but, as a special exception, it need not
include source code for modules which are standard libraries that
accompany the operating system on which the executable file runs.

  4. You may not copy, sublicense, distribute or transfer the APM
Library except as expressly provided under this License Agreement.
Any attempt otherwise to copy, sublicense, distribute or transfer the
APM Library is void and your rights to use the APM Library under this
License agreement shall be automatically terminated.  However, parties
who have received computer software programs from you with this
License Agreement will not have their licenses terminated so long as
such parties remain in full compliance.

  5. If you wish to incorporate parts of the APM Library into other
programs whose distribution conditions are different, write to Lloyd
Zusman at Master Byte Software.  We have not yet worked out a simple
rule that can be stated here, but we will often permit this.  We will
be guided by the goals of (1) preserving the free status of all
derivatives of our free software; of (2) promoting the sharing and
reuse of software; and of (3) not allowing anyone to profit from the
use of our software without us also having the opportunity to share
in these profits.

Your comments and suggestions about our licensing policies and our
software are welcome!  Please contact Lloyd Zusman, Master Byte
Software, 127 Wilder Ave., Los Gatos, California 95030, or call
(408) 395-5693.

			   NO WARRANTY

  BECAUSE THE APM LIBRARY IS LICENSED FREE OF CHARGE, WE PROVIDE
ABSOLUTELY NO WARRANTY, TO THE EXTENT PERMITTED BY APPLICABLE STATE
LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING, MASTER BYTE SOFTWARE,
LLOYD ZUSMAN AND/OR OTHER PARTIES PROVIDE THE APM LIBRARY "AS IS"
WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY
AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE THE APM
LIBRARY PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY
SERVICING, REPAIR OR CORRECTION.

  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW WILL MASTER BYTE
SOFTWARE, LLOYD ZUSMAN, AND/OR ANY OTHER PARTY WHO MAY MODIFY AND
REDISTRIBUTE THE APM LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU FOR
DAMAGES, INCLUDING ANY LOST PROFITS, LOST MONIES, OR OTHER SPECIAL,
INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR
INABILITY TO USE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA
BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY THIRD PARTIES OR A
FAILURE OF THE PROGRAM TO OPERATE WITH PROGRAMS NOT DISTRIBUTED BY
MASTER BYTE SOFTWARE) THE PROGRAM, EVEN IF YOU HAVE BEEN ADVISED OF
THE POSSIBILITY OF SUCH DAMAGES, OR FOR ANY CLAIM BY ANY OTHER PARTY.

******************************************************************************/


/*
 * Memory allocation routines for the APM library.
 *
 * $Log:	memory.c,v $
 * Revision 1.0  88/10/05  12:38:13  ljz
 * Initial release.
 *
 */

#include "apm.h"
#include "apmlocal.h"

#define NULL (void *)0

typedef struct {
	APM apm;
	unsigned int flags;
} APMMEMTAB;

#define MEMAVAIL	1

static APMMEMTAB *APM_memtab = (APMMEMTAB *)NULL;
static int APM_memnum = 0;

#ifndef MSC
extern char *malloc();
extern char *realloc();
extern void free();
#endif /* MSC */

char *
APM_alloc_mem(mem, nelem, elsize)
void *mem;
unsigned int nelem;
unsigned int elsize;
{
	char *cp;
	unsigned int size;

	apm_errno = APM_OK;

	if (elsize == 0) {
		elsize = 1;	/* default to 1 for ease of use */
	}

	if (nelem == 0) {	/* malloc(0) & realloc(MEM, 0) not portable */
		nelem = 1;
	}

	size = nelem * elsize;
	if (size < nelem || size < elsize) {	/* overflow */
		(void)APM_set_errno(APM_EPARM);
		return (NULL);
	}

	if (mem == NULL) {
		cp = malloc(size);
	}
	else {
		cp = realloc(mem, size);
	}

	if (cp == NULL) {
		(void)APM_set_errno(APM_ENOMEM);
	}

	return (cp);
}

int
APM_free_mem(mem)
void *mem;
{
	if (mem == NULL) {
		return (APM_set_errno(APM_WNULL));
	}
	free(mem);

	return (APM_OK);
}

APM
APM_do_alloc()
{
	APM apm;

	apm_errno = APM_OK;

	apm = (APM)APM_alloc_mem(NULL, 1, sizeof (struct apm_struct));
	if (apm == (APM)NULL) {
		APM_set_errno(APM_ENOMEM);
		return ((APM)NULL);
	}
	apm->magic = APM_MAGIC;
	apm->sign = 1;
	apm->base = 0;
	apm->length = 0;
	apm->dp = 0;
	apm->alloclen = 0;
	apm->data = NULL;

	return (apm);
}

APM
APM_alloc()
{
	APM apm;
	int i;
	int slot = -1;

	/*
	 * Go through allocation table to see if we have an available
	 * APM value.  If so, set its state to "not available" and then
	 * return it.
	 */
	for (i = 0; i < APM_memnum; ++i) {
		if ((APM_memtab[i].flags & MEMAVAIL) != 0) {
			if (APM_memtab[i].apm != (APM)NULL) {
				APM_memtab[i].flags = 0;
				apm = APM_memtab[i].apm;
				apm->magic = APM_MAGIC;
				apm->sign = 1;
				apm->base = 0;
				apm->length = 0;
				apm->dp = 0;
				for (i = 0; i < apm->alloclen; ++i) {
					apm->data[i] = 0;
				}
				return (apm);
			}
		}
	}

	/*
	 * If we're here, nothing is available.  We now see if there is
	 * an empty slot.
	 */
	for (i = 0; i < APM_memnum; ++i) {
		if (APM_memtab[i].apm == (APM)NULL) {
			slot = i;
			break;
		}
	}

	/*
	 * If we're here, we couldn't find an unused, pre-allocated value.
	 * If there's an available slot, allocate a value; otherwise,
	 * increase the size of the table and create a slot.
	 */
	if (slot < 0) {
		APMMEMTAB *mt;
		int newmem = APM_memnum;
		slot = newmem;
		newmem += TABINCR;
		mt = (APMMEMTAB *)APM_alloc_mem(APM_memtab, newmem,
					     sizeof (APMMEMTAB));
		if (mt == (APMMEMTAB *)NULL) {
			APM_set_errno(APM_ENOMEM);
			return ((APM)NULL);
		}
		APM_memtab = mt;
		APM_memnum = newmem;
		for (i = slot; i < APM_memnum; ++i) {
			APM_memtab[i].flags = 0;
			APM_memtab[i].apm = (APM)NULL;
		}
	}

	/*
	 * We now can allocate the new APM value.
	 */
	apm = APM_do_alloc();
	if (apm != (APM)NULL) {
		APM_memtab[slot].flags = 0;
		APM_memtab[slot].apm = apm;
	}

	return (apm);
}

int
APM_do_free(apm)
APM apm;
{
	int ercode;

	apm_errno = APM_OK;

	if (apm == (APM)NULL) {
		return (APM_WNULL);
	}

	apm->magic = 0L;

	if (apm->data != NULL && apm->alloclen > 0) {
		ercode = APM_free_mem((char *)apm->data);
		if (ercode != APM_OK) {
			return (ercode);
		}
	}

	return (APM_free_mem(apm));
}

int
APM_garbage_collect()
{
	int i;
	int ercode = APM_OK;
	int code;

	apm_errno = APM_OK;

	for (i = 0; i < APM_memnum; ++i) {
		if (APM_memtab[i].apm != (APM)NULL) {
			if ((APM_memtab[i].flags & MEMAVAIL) != 0) {
				code = APM_do_free(APM_memtab[i].apm);
				APM_memtab[i].apm = (APM)NULL;
				APM_memtab[i].flags = 0;
				if (code != APM_OK) {
					ercode = code;
				}
			}
		}
	}

	return (APM_set_errno(ercode));
}

int
APM_free(apm)
APM apm;
{
	int i;

	apm_errno = APM_OK;

	if (apm == (APM)NULL) {
		return (APM_set_errno(APM_WNULL));
	}

	for (i = 0; i < APM_memnum; ++i) {
		if (APM_memtab[i].apm == apm) {
			if ((APM_memtab[i].flags & MEMAVAIL) == 0) {
				APM_memtab[i].flags |= MEMAVAIL;
				return (APM_set_errno(APM_OK));
			}
		}
	}

	return (APM_set_errno(APM_WNOALLOC));
}
